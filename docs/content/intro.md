---
title: intro
---

# slidescape

## rationale

We need a slide authoring tool :
* that has low graphical constrains (free layout, mouse design based),
* that is productive (anyone can start producing slides in minutes),
* that has basic animation features

The slides are vector rendered in a web page.

## docs

Documentation is a [docus](https://docus.dev) project :

```
cd docs
pnpm install
pnpm run dev
```

## TODO

* [ ] publish to gitlab pages
* [ ] make a into SVG anim
* [ ] publish to npm
* [ ] add a MIT license
* [ ] integrate into [slidev](https://sli.dev/)
